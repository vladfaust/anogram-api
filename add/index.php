<?
include_once "../scripts/common.php";
include_once "../scripts/security.php";

if (in_array($_SERVER['HTTP_ORIGIN'], $GLOBALS["ALLOWED_DOMAINS"]))
{
    header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
}

mb_internal_encoding("UTF-8");
?>

<?
function getUserAvatar ($user_id, $post_id)
{
    $row = mysql_fetch_array(mysql_query("
        SELECT avatars
        FROM posts
        WHERE id=".$post_id));

    if ($row["avatars"] != null)
    {
        $avatars = unserialize(gzuncompress($row["avatars"]));
        $users = $avatars[0];
        $icons = $avatars[1];
    }
    else
    {
        $avatars = array();
        $users = array();
        $icons = array();
    }

    /*
        avatars = {
            (0)(users): [],
            (1)(icons): []
        }
    */

    // Проверяем наличие пользователя в массиве аватаров
    $num = array_search($user_id, $users);
    if ($num !== FALSE)
    {
        return $icons[$num];
    }

    // Если пользователь отсутствует, добавляем его
    else
    {
        $avatar_id = rand(1, $GLOBALS["TOTAL_AVATARS"]);

        while (in_array($avatar_id, $icons))
            $avatar_id = rand(1, $GLOBALS["TOTAL_AVATARS"]);

        array_push($users, $user_id);
        array_push($icons, $avatar_id);

        $avatars = array();
        array_push($avatars, $users);
        array_push($avatars, $icons);

        mysql_query("
            UPDATE posts
            SET avatars='".gzcompress(serialize($avatars))."'
            WHERE id=".$post_id);

        return $avatar_id;
    }
}

function verifyPosting ($user_id)
{
    $row = mysql_fetch_array(mysql_query("
        SELECT lastposted
        FROM users
        WHERE id=".$user_id));
    if (($row["lastposted"] + $GLOBALS["POSTING_BREAKTIME"]) <= time())
    {
        return true;
    }
    else return false;
}

function updatePosting ($user_id)
{
    mysql_query("
        UPDATE users
        SET lastposted=".time()."
        WHERE id=".$user_id);
}

function verifyCommenting ($user_id)
{
    $row = mysql_fetch_array(mysql_query("
        SELECT lastcommented
        FROM users
        WHERE id=".$user_id));
    if (($row["lastcommented"] + $GLOBALS["COMMENTING_BREAKTIME"]) <= time())
        return true;
    else
        return false;
}

function updateCommenting ($user_id)
{
    mysql_query("
        UPDATE users
        SET lastcommented=".time()."
        WHERE id=".$user_id);
}
?>

<?
if (!$json = json_decode(file_get_contents('php://input'))) $json = json_decode($_POST["json"]);

if (!Security::readJSON($json))
{
    Security::echoJSON(array("error"=>"Cannot read JSON.", "errorcode"=>1), false);
}

$auth_token = mysql_real_escape_string($json->auth_token);
$user_id = (int)mysql_real_escape_string($json->user_id);

switch (Security::verifyAuthToken($user_id, $auth_token))
{
    case 0:
        Security::echoJSON(array("error"=>"Authentication failed.", "errorcode"=>2), false);
        break;
    case -1:
        Security::echoJSON(array("error"=>"User is banned.", "errorcode"=>99), false);
        break;
}

include_once "../scripts/geo.php";
include_once "../scripts/upload.php";
include_once "../scripts/connect_to_db.php";
include_once "../scripts/counters.php";

$text = base64_decode(mysql_real_escape_string($json->text));
$text = Security::formatstr($text);

$content = mysql_real_escape_string($json->file);
$contenttype = mysql_real_escape_string($json->filetype);

// Проверка на наличие контента
if (empty($content) && empty($text))
{
    Security::echoJSON(array("error"=>"Not enough data.", "errorcode"=>3), false);
}

$post_id = mysql_real_escape_string($json->post_id);

if (!empty($post_id)) // Если есть post_id, то это - комментарий
{
    if (!verifyCommenting($user_id))
    {
        Security::echoJSON(array("error"=>"Take a pause.", "errorcode"=>4), false);
    }

    if ($text)
    {
        if (mb_strlen($text, "UTF-8") > $GLOBALS["MAX_TEXT_LENGTH_COMMENT"])
        {
            Security::echoJSON(array("error"=>"Too many characters.", "errorcode"=>5), false);
        }
    }
    $text = base64_encode($text);

    $file = array();

    if (!empty($content))
    {
        $file = uploadFile($content, $contenttype, $user_id);
        if (empty($file) || !empty($file["error"]))
        {
            Security::echoJSON(array("error"=>$file["error"], "errorcode"=>6), false);
        }
    }

    if (mysql_query("
        INSERT INTO comments_".$post_id."
        (user_id,text,content_link,content_type,avatar)
        VALUES('".$user_id."','".$text."','".$file["link"]."','".$file["type"]."','".getUserAvatar($user_id, $post_id)."')"))
    {
        Counters::addComment($post_id, $user_id);
        updateCommenting($user_id);
        Security::echoJSON(array("comment_id"=>mysql_insert_id()), false); // SUCCESS!
    }
    else Security::echoJSON(array("error"=>"Wrong post ID.", "errorcode"=>7), false); // Не получается сделать INSERT
}
else // Это запись, а не комментарий
{
    if (!verifyPosting($user_id))
    {
        Security::echoJSON(array("error"=>"Take a pause.", "errorcode"=>8), false);
    }

    $latitude = number_format((float)mysql_real_escape_string($json->latitude), 6);
    $longitude = number_format((float)mysql_real_escape_string($json->longitude), 6);

    if ((int)$latitude == 0 && (int)$longitude == 0)
    {
        Security::echoJSON(array("error"=>"Not enough data.", "errorcode"=>9), false);
    }

    $adress = Geo::getAdress($latitude, $longitude, $user_id);

    $file = array();

    if (!empty($content))
    {
        $file = uploadFile($content, $contenttype, $user_id);
        if (empty($file) || !empty($file["error"]))
        {
            Security::echoJSON(array("error"=>$file["error"], "errorcode"=>10), false);
        }
    }

    mysql_query("
        INSERT INTO posts
        (user_id,content_link,content_type,time,last_interacted,latitude,longitude,city,country)
        VALUES('".$user_id."','".$file["link"]."','".$file["type"]."','".time()."','".time()."','".$latitude."','".$longitude."','".$adress["city"]."','".$adress["country"]."')");

    $insert = mysql_insert_id();

    mysql_query("
    CREATE TABLE `".$GLOBALS["DATABASE_NAME"]."`.`comments_".$insert."`
    ( `id` INT NOT NULL AUTO_INCREMENT ,  `user_id` INT NOT NULL DEFAULT '0' ,  `text` TEXT NOT NULL ,`karma` INT NOT NULL DEFAULT '0', `content_link` VARCHAR(255) NOT NULL,  `content_type` INT NOT NULL DEFAULT '0', `avatar` INT NOT NULL DEFAULT '0', `likes` INT NOT NULL DEFAULT '0' , `likers` BLOB NOT NULL , `hides` INT NOT NULL DEFAULT '0' ,    PRIMARY KEY  (`id`) ) ENGINE = MyISAM;");

    Counters::addPost($user_id);
    updatePosting($user_id);
    Security::echoJSON(array("post_id"=>$insert), false);
}
?>