<?
include_once "../scripts/common.php";
include_once "../scripts/connect_to_db.php";
include_once "../scripts/security.php";

if (in_array($_SERVER['HTTP_ORIGIN'], $GLOBALS["ALLOWED_DOMAINS"]))
{
    header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
}
?>

<?
if (!$json = json_decode(file_get_contents('php://input'))) $json = json_decode($_POST["json"]);

if (!Security::readJSON($json))
{
    Security::echoJSON(array("error"=>"Cannot read JSON.", "errorcode"=>1), false);
}

$auth_token = mysql_real_escape_string($json->auth_token);
$user_id = (int)mysql_real_escape_string($json->user_id);

switch (Security::verifyAuthToken($user_id, $auth_token))
{
    case 0:
        Security::echoJSON(array("error"=>"Authentication failed.", "errorcode"=>2), false);
        break;
    case -1:
        Security::echoJSON(array("error"=>"User is banned.", "errorcode"=>99), false);
        break;
}

$action = mysql_real_escape_string($json->action);

if ($action == "post")
{
    $encrypted = mysql_real_escape_string($json->data);

    if (!$encrypted)
        Security::echoJSON(array("error"=>"Not enough data.", "errorcode"=>3), false);

    if (!$decrypted = Security::decryptPMData($encrypted, $user_id))
        Security::echoJSON(array("error"=>"Decryption failed.", "errorcode"=>4), false);

    $from = $user_id;

    $post_id = mysql_real_escape_string($decrypted->post_id);
    $comment_id = mysql_real_escape_string($decrypted->comment_id);
    $to = mysql_real_escape_string($decrypted->to);

    if ($post_id)
    {
        if ($comment_id)
        {
            if ($row = mysql_fetch_array(mysql_query("
                SELECT user_id
                FROM comments_".$post_id."
                WHERE id=".$comment_id)))
                $to = $row["user_id"];
            else
                Security::echoJSON(array("error"=>"Cannot find user.", "errorcode"=>9), $gzip);
        }
        else
        {
            if ($row = mysql_fetch_array(mysql_query("
                SELECT user_id
                FROM posts
                WHERE id=".$post_id)))
                $to = $row["user_id"];
            else
                Security::echoJSON(array("error"=>"Cannot find user.", "errorcode"=>10), $gzip);
        }
    }

    $content = mysql_real_escape_string($decrypted->content);
    if (!$content = Security::encryptPMData($content, $to))
        Security::echoJSON(array("error"=>"Encryption failed.", "errorcode"=>5), false);

    if (!mysql_query("
        INSERT INTO pm
        (sender, receiver, content, time)
        VALUES ('".$from."', '".$to."', '".$content."', '".time()."')"))
        Security::echoJSON(array("error"=>"Insert failed.", "errorcode"=>6), false);

    Security::echoJSON(array("success"=>"OK", "errorcode"=>7), false);
}
elseif ($action == "check")
{
    $row = mysql_fetch_array(mysql_query("
        SELECT last_pm_receive
        FROM users
        WHERE id=".$user_id));

    $total = mysql_num_rows(mysql_query("
        SELECT time
        FROM pm
        WHERE receiver=".$user_id." AND time>".$row["last_pm_receive"]));

    Security::echoJSON(array("new"=>$total), false);
}
elseif ($action == "receive")
{
    $result = mysql_query("
        SELECT *
        FROM pm
        WHERE receiver=".$user_id."
        ORDER BY time DESC");

    $pms = array();
    $json = array("total"=>0);

    while ($row = mysql_fetch_array($result))
    {
        array_push($pms, array(
            "from"=>$row["sender"],
            "content"=>$row["content"],
            "time"=>$row["time"]));
        $json["total"]++;
        mysql_query("
            DELETE FROM pm
            WHERE id=".$row["id"]);
    }

    $json["pms"] = $pms;

    mysql_query("
        UPDATE users
        SET last_pm_receive=".time()."
        WHERE id=".$user_id);

    Security::echoJSON($json, false);
}
else
    Security::echoJSON(array("error"=>"Wrong action.", "errorcode"=>8), false);
?>