<?
include_once "../scripts/common.php";
include_once "../scripts/connect_to_db.php";
include_once "../scripts/counters.php";
include_once "../scripts/security.php";

if (in_array($_SERVER['HTTP_ORIGIN'], $GLOBALS["ALLOWED_DOMAINS"]))
{
    header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
}
?>

<?
if (!$json = json_decode(file_get_contents('php://input'))) $json = json_decode($_POST["json"]);

if (!Security::readJSON($json))
{
    Security::echoJSON(array("error"=>"Cannot read JSON.", "errorcode"=>1), false);
}

$auth_token = mysql_real_escape_string($json->auth_token);
$user_id = (int)mysql_real_escape_string($json->user_id);

switch (Security::verifyAuthToken($user_id, $auth_token))
{
case 0:
    Security::echoJSON(array("error"=>"Authentication failed.", "errorcode"=>2), false);
    break;
case -1:
    Security::echoJSON(array("error"=>"User is banned.", "errorcode"=>11), false);
    break;
}

$post_id = mysql_real_escape_string($json->post_id);

if (empty($post_id))
{
    Security::echoJSON(array("error"=>"Not enough data.", "errorcode"=>3), false);
}

$comment_id = mysql_real_escape_string($json->comment_id);

// В зависимости от записи или комментария, достаем инфо о лайках и авторе
if (!empty($comment_id)) $query = "
    SELECT user_id, likes, likers
    FROM comments_".$post_id."
    WHERE id=".$comment_id;
else $query = "
    SELECT user_id, likes, likers
    FROM posts
    WHERE id=".$post_id;
// ==============================================================

if ($row = mysql_fetch_array(mysql_query($query)))
{
    // Если лайков > 0, достаем лайкеров из БД, иначе - создаем новый массив
    if ($row["likes"] != 0) $array = unserialize(gzuncompress($row["likers"]));
    else $array = array();

    $liked = false;
    $likes = count($array);

    if (!in_array($user_id, $array))
    {
        $likes = array_push($array, $user_id);
        Counters::like($array, $post_id, $comment_id, $row["user_id"], 1);
        $liked = true;
    }
    else
    {
        if($key = array_search($user_id, $array))
        {
            unset($array[$key]);
        }
        $likes -= 1;
        Counters::like($array, $post_id, $comment_id, $row["user_id"], -1);
    }

    Security::echoJSON(array("likes"=>$likes, "liked"=>$liked), false);
}
else Security::echoJSON(array("error"=>"Wrong post or comment ID.", "errorcode"=>4), false);
?>