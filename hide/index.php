<?
	include_once "../scripts/common.php";

	if (in_array($_SERVER['HTTP_ORIGIN'], $GLOBALS["ALLOWED_DOMAINS"]))
	{  
		header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
	}
?>

<?
	if (!$json = json_decode(file_get_contents('php://input'))) $json = json_decode($_POST["json"]);
	
	if (!(isset($json) && !empty($json)))
	{
		Security::echoJSON(array("error"=>"No JSON passed", "errorcode"=>1), false);
	}
	
	include_once "../scripts/connect_to_db.php";
	include_once "../scripts/counters.php";
	include_once "../scripts/security.php";

	$user_id = mysql_real_escape_string($json->user_id);
	$auth_token = mysql_real_escape_string($json->auth_token);
	
	if (!Security::verifyAuthToken($user_id, $auth_token))
	{
		Security::echoJSON(array("error"=>"Wrong session id", "errorcode"=>2), false);
	}
	
	$post_id = mysql_real_escape_string($json->post_id);
	
	if (empty($post_id))
	{
		Security::echoJSON(array("error"=>"Not enough data", "errorcode"=>3), false);
	}
	
	$comment_id = mysql_real_escape_string($json->comment_id);
	
	if (!empty($comment_id)) $query = "
		SELECT user_id, content_link, content_type
		FROM comments_".$post_id."
		WHERE id=".$comment_id;
	else $query = "
		SELECT user_id, content_link, content_type
		FROM posts
		WHERE id=".$post_id;
	
	if ($row = mysql_fetch_array(mysql_query($query)))
	{
		$complain = (int)mysql_real_escape_string($json->complain);
		if ($row["content_link"] != "")
		{
			copy($row["content_link"], $GLOBALS["HOST"]."/complains".parse_url($row["content_link"], PHP_URL_PATH));
			$query = "
				INSERT INTO complains
				(content_link, content_type, complain, post_id, comment_id, user_id)
				VALUES('".$GLOBALS["HOST"]."/complains".parse_url($row["content_link"], PHP_URL_PATH)."','".$row["content_type"]."','".$complain."','".$post_id."','".$comment_id."','".$row["user_id"]."')";
		}
		else
			$query = "
				INSERT INTO complains
				(complain, post_id, comment_id, user_id)
				VALUES('".$complain."','".$post_id."','".$comment_id."','".$row["user_id"]."')";

        Counters::addHide($row["user_id"], $comment_id, $post_id);
	    Security::echoJSON(array("success"=>"OK"), false);
	}
	else Security::echoJSON(array("error"=>"Wrong post or comment ID", "errorcode"=>4), false); // Неправильные запись или комментарий
?>