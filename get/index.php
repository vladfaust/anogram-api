<?
include_once "../scripts/common.php";
include_once "../scripts/connect_to_db.php";
include_once "../scripts/security.php";
include_once "../scripts/geo.php";

if (in_array($_SERVER['HTTP_ORIGIN'], $GLOBALS["ALLOWED_DOMAINS"]))
{
    header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
}
?>

<?
// Узнать, стоит ли пользовательский лайк
function getLiked ($row, $user_id)
{
    if ($row["likes"]>0)
    {
        $array = unserialize(gzuncompress($row["likers"]));
    }
    if (in_array($user_id, $array)) return true;
    else return false;
}

if (!$json = json_decode(file_get_contents('php://input'))) $json = json_decode($_POST["json"]);

if (!Security::readJSON($json))
{
    Security::echoJSON(array("error"=>"Cannot read JSON.", "errorcode"=>1), false);
}

$user_id = mysql_real_escape_string($json->user_id);
$auth_token = mysql_real_escape_string($json->auth_token);
$gzip = (bool)mysql_real_escape_string($json->gzip);

switch (Security::verifyAuthToken($user_id, $auth_token))
{
    case 0:
        Security::echoJSON(array("error"=>"Authentication failed.", "errorcode"=>2), false);
        break;
    case -1:
        Security::echoJSON(array("error"=>"User is banned.", "errorcode"=>99), false);
        break;
}

$action = mysql_real_escape_string($json->action);

if (empty($action))
{
    Security::echoJSON(array("error"=>"Not enough data.", "errorcode"=>3), $gzip);
}

if ($action == "nearby")
{
    $latitude = number_format((float)mysql_real_escape_string($json->latitude), 6);
    $longitude = number_format((float)mysql_real_escape_string($json->longitude), 6);

    $offset = (int)mysql_real_escape_string($json->offset);

    $adress = Geo::getAdress($latitude, $longitude, $user_id);

    if (!$adress)
    {
        $adress = array("city"=>"???", "country"=>"???");
    }

    (bool)$silent = mysql_real_escape_string($json->silent);
    $json = array("total"=>0, "new"=>0);
    $posts = array();

    $user_row = mysql_fetch_array(mysql_query("
        SELECT last_scan_".$action."
        FROM users
        WHERE id=".$user_id));

    // Ищем записи по городу и стране
    $query = "
        SELECT *
        FROM posts
        WHERE city='".$adress["city"]."'
        ORDER BY time DESC
        LIMIT ".$offset.", ".$GLOBALS["POSTS_IN_RESPONSE"];

    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result))
    {
        $dist = -1;

        if (($latitude || $longitude) && ($row["latitude"] || $row["longitude"]))
        {
            $dist = Geo::getDistance($row["latitude"], $row["longitude"], $latitude, $longitude);
            $dist = floor($dist);
        }

        if ($dist != -1 && $dist < $GLOBALS["SCAN_RADIUS"])
        {
            if (!$silent) // Если это не "тихое" обновление, писать весь массив
            {
                $liked = false;

                $karma_query = mysql_fetch_array(mysql_query("
                    SELECT karma
                    FROM users
                    WHERE id=".$row["user_id"]));

                array_push($posts, array(
                    "post_id"=>$row["id"],
                    "content_link"=>$row["content_link"],
                    "content_type"=>$row["content_type"],
                    "dist"=>$dist,
                    "time"=>(time() - $row["time"]),
                    "likes"=>$row["likes"],
                    "liked"=>getLiked($row, $user_id),
                    "comments"=>$row["comments"],
                    "karma"=>$karma_query["karma"]));
            }
            else
            {
                array_push($posts, $row["id"]);
                if ($user_row["last_scan_".$action] <= $row["time"])
                    $json["new"]++;
            }
            $json["total"]++;
        }
    }

    $json["posts"] = $posts;

    // Обновляем время последнего обновления записей
    if (!$silent && !$offset) mysql_query("
        UPDATE users
        SET last_scan_".$action."=".time()."
        WHERE id=".$user_id);
}
elseif ($action == "top") // Показать топ
{
    $latitude = number_format((float)mysql_real_escape_string($json->latitude), 6);
    $longitude = number_format((float)mysql_real_escape_string($json->longitude), 6);
    $offset = (int)mysql_real_escape_string($json->offset);

    $json = array("total"=>0, "new"=>0);
    $posts = array();

    $query = "
        SELECT *
        FROM posts
        ORDER BY likes DESC
        LIMIT ".$offset.", ".$GLOBALS["POSTS_IN_RESPONSE"];

    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result))
    {
        $locality = "";
        if ($row["city"])
        {
            if ($row["city"] && $row["city"] != "???") $locality.=$row["city"].", ";
        }

        $dist = -1;
        if (($latitude || $longitude) && ($row["latitude"] || $row["longitude"]))
        {
            $dist = Geo::getDistance($row["latitude"], $row["longitude"], $latitude, $longitude);
            $dist = floor($dist);
        }
        if ($dist > $GLOBALS["IAM_IN_YOUR_CITY"]) $dist = -1;

        $karma_query = mysql_fetch_array(mysql_query("
            SELECT karma
            FROM users
            WHERE id=".$row["user_id"]));

        array_push($posts, array(
            "post_id"=>$row["id"],
            "content_link"=>$row["content_link"],
            "content_type"=>$row["content_type"],
            "locality"=>$locality,
            "dist"=>$dist,
            "time"=>(time() - $row["time"]),
            "likes"=>$row["likes"],
            "liked"=>getLiked($row, $user_id),
            "comments"=>$row["comments"],
            "karma"=>$karma_query["karma"]));
        $json["total"]++;
    }

    $json["posts"] = $posts;
}
elseif ($action == "comments") // Показать комментарии
{
    $post_id = mysql_real_escape_string($json->post_id);

    if (empty($post_id))
    {
        Security::echoJSON(array("error"=>"Not enough data.", "errorcode"=>4), $gzip);
    }

    $offset = (int)mysql_real_escape_string($json->offset);

    $query = "
        SELECT *
        FROM comments_".$post_id."
        ORDER BY id
        LIMIT ".$offset.", ".$GLOBALS["COMMENTS_IN_RESPONSE"];

    if ($result = mysql_query($query))
    {

        $json = array("total"=>0);
        $comments = array();

        while ($row = mysql_fetch_array($result))
        {
            $karma_query = mysql_fetch_array(mysql_query("
                SELECT karma
                FROM users
                WHERE id=".$row["user_id"]));

            array_push($comments, array(
                "comment_id"=>$row["id"],
                "avatar"=>$row["avatar"],
                "text"=>$row["text"],
                "content_link"=>$row["content_link"],
                "content_type"=>$row["content_type"],
                "likes"=>$row["likes"],
                "liked"=>getLiked($row, $user_id),
                "karma"=>$karma_query["karma"]));
            $json["total"]++;
        }
        $json["comments"] = $comments;
    }
    else Security::echoJSON(array("error"=>"Wrong post ID.", "errorcode"=>5), $gzip);
}
elseif ($action == "post") // Загрузить определенный пост
{
    $post_id = mysql_real_escape_string($json->post_id);

    $latitude = number_format((float)mysql_real_escape_string($json->latitude), 6);
    $longitude = number_format((float)mysql_real_escape_string($json->longitude), 6);

    if (empty($post_id))
    {
        Security::echoJSON(array("error"=>"Not enough data.", "errorcode"=>6), $gzip);
    }

    // Ищем запись
    $result = mysql_query("
        SELECT *
        FROM posts
        WHERE id=".$post_id);

    if ($row = mysql_fetch_array($result))
    {
        $dist = -1;
        if (($latitude || $longitude) && ($row["latitude"] || $row["longitude"]))
        {
            $dist = Geo::getDistance($row["latitude"], $row["longitude"], $latitude, $longitude);
            $dist = floor($dist);
        }
        if ($dist > $GLOBALS["IAM_IN_YOUR_CITY"]) $dist = -1;

        $locality = "";
        if ($dist == -1 && $row["city"] && $row["city"] != "???")
        {
            $locality.=$row["city"];
        }

        $liked = false;
        $json =  array(
            "post_id"=>$row["id"],
            "text"=>$row["text"],
            "content_link"=>$row["content_link"],
            "content_type"=>$row["content_type"],
            "dist"=>$dist,
            "locality"=>$locality,
            "time"=>(time() - $row["time"]),
            "likes"=>$row["likes"],
            "liked"=>getLiked($row, $user_id),
            "comments"=>$row["comments"]);
    }
    else Security::echoJSON(array("error"=>"Wrong post ID.", "errorcode"=>7), $gzip);
}
elseif ($action == "user")
{
    $post_id = mysql_real_escape_string($json->post_id);
    $comment_id = mysql_real_escape_string($json->comment_id);

    $fields = mysql_real_escape_string($json->fields);

    if ($post_id)
    {
        if ($comment_id)
        {
            if ($row = mysql_fetch_array(mysql_query("
                SELECT user_id
                FROM comments_".$post_id."
                WHERE id=".$comment_id)))
                $user_id = $row["user_id"];
            else
                Security::echoJSON(array("error"=>"Cannot find user by id.", "errorcode"=>8), $gzip);
        }
        else
        {
            if ($row = mysql_fetch_array(mysql_query("
                SELECT user_id
                FROM posts
                WHERE id=".$post_id)))
                $user_id = $row["user_id"];
            else
                Security::echoJSON(array("error"=>"Cannot find user by id.", "errorcode"=>9), $gzip);
        }
    }

    if ($row = mysql_fetch_array("
            SELECT *
            FROM users
            WHERE id=".$user_id))
    {
        $json = array();

        if (!(strrpos($fields, "karma") === FALSE))
            $json["karma"] = $row["karma"];
        if (!(strrpos($fields, "id") === FALSE))
            $json["id"] = $row["id"];
    }
    else
        Security::echoJSON(array("error"=>"Cannot find user by id.", "errorcode"=>10), $gzip);
}
else Security::echoJSON(array("error"=>"Wrong action.", "errorcode"=>11), $gzip);

Security::echoJSON($json, $gzip);
?>