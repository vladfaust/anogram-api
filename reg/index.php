<?
include_once "../scripts/common.php";

if (in_array($_SERVER['HTTP_ORIGIN'], $GLOBALS["ALLOWED_DOMAINS"]))
{
    header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
}
?>

<?
if (!$json = json_decode(file_get_contents('php://input'))) $json = json_decode($_POST["json"]);

if (!(isset($json) && !empty($json)))
{
    Security::echoJSON(array("error"=>"No JSON was passed.", "errorcode"=>1), false);
}

include_once "../scripts/connect_to_db.php";
include_once "../scripts/security.php";

$uuid = mysql_real_escape_string($json->uuid);

if (!($uuid = Security::encryptUUID($uuid)))
{
    Security::echoJSON(array("error"=>"Wrong UUID format.", "errorcode"=>2), false);
}

$auth_token = Security::generateAuthToken($uuid);
$pmkey = Security::generatePMKey($uuid);

$row = mysql_fetch_array(mysql_query("
    SELECT id, auth_token, banned
    FROM users
    WHERE uuid='".$uuid."'"));

if (!$row)
{
    mysql_query("
        INSERT INTO users
        (uuid, auth_token, pmkey)
        VALUES('".$uuid."', '".$auth_token."', '".$pmkey."')");

    Security::echoJSON(array(
        "user_id"=>mysql_insert_id(),
        "auth_token"=>$auth_token,
        "pmkey"=>Security::wrapPMKey($pmkey, $auth_token)), false);
}
else
{
    if (!$row["banned"])
    {
        mysql_query("
            UPDATE users
            SET pmkey='".$pmkey."', auth_token='".$auth_token."'
            WHERE id=".$row["id"]);

        Security::echoJSON(array(
            "user_id"=>$row["id"],
            "auth_token"=>$auth_token,
            "pmkey"=>Security::wrapPMKey($pmkey, $auth_token)), false);
    }
    else
        Security::echoJSON(array("error"=>"User is banned.", "errorcode"=>99), false);
}
?>