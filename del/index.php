<?
include_once "../scripts/common.php";
include_once "../scripts/connect_to_db.php";
include_once "../scripts/security.php";

if (in_array($_SERVER['HTTP_ORIGIN'], $GLOBALS["ALLOWED_DOMAINS"]))
{
    header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
}
?>

<?
if (!$json = json_decode(file_get_contents('php://input'))) $json = json_decode($_POST["json"]);

if (!Security::readJSON($json))
{
    Security::echoJSON(array("error"=>"Cannot read JSON.", "errorcode"=>1), false);
}

$user_id = mysql_real_escape_string($json->user_id);
$auth_token = mysql_real_escape_string($json->auth_token);

switch (Security::verifyAuthToken($user_id, $auth_token))
{
    case 0:
        Security::echoJSON(array("error"=>"Authentication failed.", "errorcode"=>2), false);
        break;
    case -1:
        Security::echoJSON(array("error"=>"User is banned.", "errorcode"=>99), false);
        break;
}

$post_id = mysql_real_escape_string($json->post_id);
$comment_id = mysql_real_escape_string($json->comment_id);

if (empty($post_id))
{
    Security::echoJSON(array("error"=>"Not enough data.", "errorcode"=>3), false);
}

if (!empty($comment_id))
    $query = "
        SELECT user_id, content_link
        FROM comments_".$post_id."
        WHERE id=".$comment_id;
else
    $query = "
        SELECT user_id, content_link
        FROM posts
        WHERE id=".$post_id;

if ($row = mysql_fetch_array(mysql_query($query)))
{
    if ($row["user_id"] == $user_id)
    {
        if (!empty($comment_id))
        {
            if (mysql_query("
                DELETE
                FROM comments_".$post_id."
                WHERE id=".$comment_id))
            {
                if ($row["content_link"] != "")
                {
                    if (!unlink($GLOBALS["FTP"].parse_url($row["content_link"], PHP_URL_PATH)))
                        Security::echoJSON(array("error"=>"MySQL error.", "errorcode"=>6), false);
                }
            }
            else
                Security::echoJSON(array("error"=>"MySQL error.", "errorcode"=>7), false);
        }
        else
        {
            if (mysql_query("
                DELETE
                FROM posts
                WHERE id=".$post_id))
            {
                if (mysql_query("
                    DROP
                    TABLE comments_".$post_id))
                {
                    if ($row["content_link"] != "")
                        if (!unlink($GLOBALS["FTP"].parse_url($row["content_link"], PHP_URL_PATH)))
                            Security::echoJSON(array("error"=>"MySQL error.", "errorcode"=>9), false);
                }
                else
                    Security::echoJSON(array("error"=>"MySQL error.", "errorcode"=>10), false);
            }
            else
                Security::echoJSON(array("error"=>"MySQL error.", "errorcode"=>8), false);
        }
    }
    else Security::echoJSON(array("error"=>"Access denied.", "errorcode"=>4), false);
}
else Security::echoJSON(array("error"=>"Cannot find post or comment by ID.", "errorcode"=>5), false);

Security::echoJSON(array("success"=>"OK"), false);
?>