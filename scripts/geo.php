<?
	include_once "counters.php";
	require_once "common.php";
	
	class Geo
	{
		// Получение города и страны по координатам. Если разница между последней координатой больше 5км, то изменить значение last_latitude, last_longitude, last_city и last_country у device.
		static public function getAdress($latitude, $longitude, $user_id)
		{
			$row = mysql_fetch_array(mysql_query("
				SELECT last_coords_sync, last_latitude, last_longitude, last_city, last_country
				FROM users
				WHERE id=".$user_id));
			
			if ((self::getDistance($latitude, $longitude, $row["last_latitude"], $row["last_longitude"]) >= $GLOBALS["MAX_DISTANCE_DIFFERENCE"]) || (($row["last_coords_sync"]+$GLOBALS["LOCATION_REFRESH_TIME"]) <= time()))
			{
				// Координаты сильно изменились или пришло время их обновления или отсутствует город
				Counters::addMapResponse();
				
				// Обращение к серверу Яндекса
				$url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=en&latlng=";
				$url = $url.urlencode($latitude.",".$longitude);	
				$resp_json = self::curl_file_get_contents($url);
				$resp = json_decode($resp_json, true);
				
				if($resp["status"] == "OK")
				{
					
					$city = "???";
					$country = "???";
					
					$temp = $resp["results"][0]["address_components"];
					
					for ($i = 0; $i < count($temp); $i++)
					{
						if ($temp[$i]["types"][0] == "locality")
						{
							$city = $temp[$i]["short_name"];
							break;
						}
					}
					
					for ($i = 0; $i < count($temp); $i++)
					{
						if ($temp[$i]["types"][0] == "country")
						{
							$country = $temp[$i]["short_name"];
							break;
						}
					}
					
					// Обновляем информацию пользователя
					mysql_query("
						UPDATE users
						SET last_coords_sync='".time()."', last_latitude='".$latitude."', last_longitude='".$longitude."', last_city='".$city."', last_country='".$country."'
						WHERE id=".$user_id);
						
					return array("city"=>$city, "country"=>$country);
				}
				else return false; // Не получилось найти город по координатам
			}
			else
			{
				// Возвращаем старые значения
				$city = $row["last_city"];
				$country = $row["last_country"];	
				
				return array("city"=>$city, "country"=>$country);
			}
		}
		
		// Часть импортного кода, лучше его не трогать
		static private function curl_file_get_contents($URL)
		{
			$c = curl_init();
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_URL, $URL);
			$contents = curl_exec($c);
			curl_close($c);
	
			if ($contents) return $contents;
				else return FALSE;
		}
		
		// Расстояние в метрах между двумя точками
		public static function getDistance($lat1, $lon1, $lat2, $lon2) 
		{
			$lat1 *= M_PI / 180;
			$lat2 *= M_PI / 180;
			$lon1 *= M_PI / 180;
			$lon2 *= M_PI / 180;
			
			$d_lon = $lon1 - $lon2;
			
			$slat1 = sin($lat1);
			$slat2 = sin($lat2);
			$clat1 = cos($lat1);
			$clat2 = cos($lat2);
			$sdelt = sin($d_lon);
			$cdelt = cos($d_lon);
			
			$y = pow($clat2 * $sdelt, 2) + pow($clat1 * $slat2 - $slat1 * $clat2 * $cdelt, 2);
			$x = $slat1 * $slat2 + $clat1 * $clat2 * $cdelt;
			
			return atan2(sqrt($y), $x) * 6372795; // 6372795 - радиус Земли в метрах
		}
	}
?>