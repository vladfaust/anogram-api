<?
	include_once("connect_to_db.php");
	
	class Counters
	{
		// 1 DBR. Добавляет в статистику информацию о запросе к стороннему API карт.
		static public function addMapResponse()
		{
			mysql_query("
				UPDATE statistics
				SET total_geocodings=(total_geocodings+1)
				WHERE date='".date("Y-m-d")."'");
		}
		
		// 1 DBR. Добавляет информацию о регистрации нового пользователя.
		static public function addUser()
		{
			mysql_query("
				UPDATE statistics
				SET new_users=(new_users+1)
				WHERE date='".date("Y-m-d")."'");
		}
		
		// 2 DBR. Добавляет информацию о размещении записи.
		static public function addPost($user_id)
		{
			mysql_query("
				UPDATE statistics
				SET total_posts=(total_posts+1)
				WHERE date='".date("Y-m-d")."'");
			mysql_query("
				UPDATE users
				SET total_posts=(total_posts + 1) 
				WHERE id=".$user_id);
		}
		
		// 3 DBR. Добавляет информацию о размещении комментария.
		static public function addComment($post_id, $user_id)
		{
			mysql_query("
				UPDATE statistics
				SET total_comments=(total_comments+1)
				WHERE date='".date("Y-m-d")."'");
			mysql_query("
				UPDATE posts
				SET comments=(comments+1), last_interacted=".time()."
				WHERE id=".mysql_real_escape_string($post_id));
			mysql_query("
				UPDATE users
				SET total_comments=(total_comments + 1) 
				WHERE id=".$user_id);
		}
		
		// 3 DBR. Изменяет лайк у записи или комментария.
		static public function like ($likers, $post_id, $comment_id, $author, $howmuch)
		{
			if ($post_id)
			{
				mysql_query("
					UPDATE statistics
					SET total_likes=(total_likes+(".$howmuch."))
					WHERE date='".date("Y-m-d")."'");
				mysql_query("
					UPDATE users
					SET total_likes=(total_likes+(".$howmuch.")), karma=(karma+(".$howmuch."))
					WHERE id=".$author);
				if ($comment_id)
				{
					mysql_query("
						UPDATE comments_".mysql_real_escape_string($post_id)." 
						SET likes=(likes+(".$howmuch.")), likers='".gzcompress(serialize($likers))."' 
						WHERE id=".mysql_real_escape_string($comment_id));
				}
				else
					mysql_query("
						UPDATE posts 
						SET likes=(likes+(".$howmuch.")), likers='".gzcompress(serialize($likers))."' 
						WHERE id=".mysql_real_escape_string($post_id));
			}
		}
		
		// 3 DBR. Добавляет информацию о скрытии записи или комментария
		static public function addHide($user_id, $comment_id, $post_id)
		{
			if (!empty($comment_id))
				$query = "
					UPDATE comments_".$post_id."
					SET hides=hides+1
					WHERE id=".$comment_id;
			else
				$query = "
					UPDATE posts
					SET hides=hides+1
					WHERE id=".$post_id;
			mysql_query("
				UPDATE users
				SET total_hides=total_hides+1, karma=(karma-1)
				WHERE id=".$user_id);
			mysql_query("
				UPDATE statistics
				SET total_hides=(total_hides+1)
				WHERE date='".date("Y-m-d")."'");
		}

        static public function addPM ($from, $to)
        {
            mysql_query("
				UPDATE statistics
				SET total_pms=(total_pms+1)
				WHERE date='".date("Y-m-d")."'");
        }
	}
?>