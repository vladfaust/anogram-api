<?
	include_once "connect_to_db.php";
	include_once "common.php";
	
	class Security
	{
        static public function echoJSON ($json, $gzip)
        {
            if ($gzip)
            {
                $base64 = base64_encode(gzencode(json_encode($json), 3));
                $convertedText = mb_convert_encoding($base64, 'utf-8', mb_detect_encoding($base64));
                $compressed = array("base64"=>$convertedText);
                echo json_encode($compressed);
            }
            else
                echo json_encode($json);
            exit;
        }

        static public function readJSON ($json)
        {
            if (isset($json) && !empty($json))
                return $json;
        }

		static public function formatstr($str) 
		{
			$str = strip_tags($str);
			$str = trim($str);
			return $str;
		}
		
		// Возвращает декодированную строку
		static public function Base64 ($base64)
		{
			$base64 = str_replace(" ", "+", $base64);
			$base64 = str_replace('\n', "", $base64);
			return base64_decode($base64);	
		}
		
		// Шифрование UUID + его валидация
		static public function encryptUUID ($uuid)
		{
			if (strlen($uuid) == 44 && substr($uuid, 0, 8) == "uncleben")
				return hash("sha512", strrev(md5($uuid)));
			else return false;
		}
		
		// Генерирует случайный AuthToken на основе UUID
		static public function generateAuthToken ($uuid)
		{
			return md5(time().rand(1, 1000).strrev($uuid)."secretWord_fakamazafaka");
		}
		
		// Проверка AuthToken
		static public function verifyAuthToken ($user_id, $auth_token)
		{
			if ($user_id != 0)
			{
				$row = mysql_fetch_array(mysql_query("
					SELECT auth_token, banned
					FROM users
					WHERE id=".$user_id));
                if ($row["banned"]) return -1;
				if ($row["auth_token"] == $auth_token) return 1;
				else return 0;
			}
			else return 0;
		}

        // Генерация PMKey
        // PrivateMessagesKey
        static public function generatePMKey ($uuid)
        {
            return hash('sha512', time().$uuid);
        }

        // Шифрование PMKey для первичной передачи клиенту
        // Дешифровка: PMKey = AES-128-CBC_decrypt(wrapped_pmkey, md5(strreverse(auth_token)));
        static public function wrapPMKey ($pmkey, $auth_token)
        {
            return openssl_encrypt($pmkey, "AES-128-CBC", md5(strrev($auth_token)));
        }

        // Расшифровка сообщения
        static public function decryptPMData ($data, $sender)
        {
            if ($row = mysql_fetch_array(mysql_query("
                SELECT pmkey
                FROM users
                WHERE id=".$sender)))
                return openssl_decrypt($data, "AES-128-CBC", $row["pmkey"]);
            else
                return false;
        }

        // Шифрование сообщения
        static public function encryptPMData ($data, $reciever)
        {
            if ($row = mysql_fetch_array(mysql_query("
                SELECT pmkey
                FROM users
                WHERE id=".$reciever)))
                return openssl_encrypt($data, "AES-128-CBC", $row["pmkey"]);
            else
                return false;
        }
	}
?>